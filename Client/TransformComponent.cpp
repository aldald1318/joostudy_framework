#include "framework.h"
#include "TransformComponent.h"

TransformComponent* TransformComponent::CreateComponent(const FPOINT& fInfo, const float& fVel, const float& fSize)
{
	TransformComponent* pInstance = new TransformComponent(fInfo, fVel, fSize);

	if(pInstance->InitializeComponent())
		return nullptr;

	return pInstance;
}

TransformComponent::TransformComponent() : m_Velocity(0.f), m_Size(0.f), m_Acceleration(0.f)
{
	ZeroMemory(&m_tInfo, sizeof(m_tInfo));
}

TransformComponent::TransformComponent(const FPOINT& fInfo, const float& fSpeed, const float& fSize)
	: m_tInfo(fInfo), m_Velocity(fSpeed), m_Size(fSize), m_Acceleration(0.f)
{
}

TransformComponent::~TransformComponent()
{
	this->ReleaseComponent();
}

HRESULT TransformComponent::InitializeComponent()
{
	return S_OK;
}

void TransformComponent::UpdateComponent(const float& fTimeDelta)
{
	m_Velocity = m_Velocity *fTimeDelta;
}

void TransformComponent::ReleaseComponent()
{
}

void TransformComponent::MoveUp()
{
	m_tInfo.y -= m_Velocity;
}

void TransformComponent::MoveDown()
{
	m_tInfo.y += m_Velocity;
}

void TransformComponent::MoveLeft()
{
	m_tInfo.x -= m_Velocity;
}

void TransformComponent::MoveRight()
{
	m_tInfo.x += m_Velocity;
}


void TransformComponent::SetPosition(const FPOINT& tInfo)
{
	m_tInfo = tInfo;
}

void TransformComponent::SetSize(const float& fSize)
{
	m_Size = fSize;
}

FPOINT TransformComponent::GetPosition() const
{
	return m_tInfo;
}

float TransformComponent::GetSize() const
{
	return m_Size;
}

