#include "framework.h"
#include "SceneLogo.h"

#include "Player.h"

SceneLogo::SceneLogo()
{
}

SceneLogo::~SceneLogo()
{
	ReleaseScene();
}

HRESULT SceneLogo::InitializeScene()
{
	// 플레이어 추가
	GameObject* pPlayer = new Player();
	pPlayer->Initialize();

	m_pObjectMgr->Add_Object(ObjectMgr::OBJID::ID_PLAYER, pPlayer);

	return S_OK;
}

void SceneLogo::UpdateScene(const float& fTimeDelta)
{
	if (m_pObjectMgr != nullptr)
		m_pObjectMgr->Update_ObjectMgr(fTimeDelta);

	//if( .. 한바퀴가 돌았을때 ..)
	//	Scene* LobbyScene=  new SceneLobby()
	//	InitializeSceene
	//	SceneMgr -> ChangeScene(LobyScene)
}

void SceneLogo::ReleaseScene()
{
	STD cout << "로고씬 릴리즈 중,,," << STD endl;
	ObjectMgr::DestroyInstance();
}
