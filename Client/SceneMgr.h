#pragma once
#include "TemplateSingleton.h"

// Scene 객체를 관리하는 관리자 객체입니다.
// Mgr은 모두 Singleton 으로 구현되어 있습니다.

// Singleton 특징
// *호출은 여러번되지만 할당은 1번만 일어난다.
// 한번이라도 호출을 했다면 꼭 Destroy를 해줘야한다.
class Scene;
class SceneMgr : public TemplateSingleton<SceneMgr>
{
public:
	// 생성자 소멸자 private
	SceneMgr();
	~SceneMgr();

public:
	HRESULT SceneChange(Scene* pNewScene);
	void UpdateScene(const float& fTimeDelta);
	Scene* GetCurScene() const;
	
private:
	Scene* m_pCurScene;
};

