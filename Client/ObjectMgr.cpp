#include "framework.h"
#include "ObjectMgr.h"

#include "GameObject.h"

ObjectMgr::ObjectMgr() {}
ObjectMgr::~ObjectMgr() 
{
	Clear_All();
}

void ObjectMgr::Add_Object(OBJID eID, GameObject* pObj)
{
	if (pObj == nullptr)
		return;

	m_ObjectList[static_cast<int>(eID)].emplace_back(pObj);
}

void ObjectMgr::Update_ObjectMgr(const float& DeltaTime)
{
	for (size_t i = 0; i < static_cast<int>(OBJID::ID_END); ++i)
	{
		for (auto& pObj : m_ObjectList[i])
		{
			int iResult = pObj->Update(DeltaTime);
		}
	}
}

void ObjectMgr::Clear_Filter(OBJID eID)
{
	for (auto& pObj : m_ObjectList[static_cast<int>(eID)])
		SAFE_DELETE_PTR(pObj);

	m_ObjectList[static_cast<int>(eID)].clear();
}

void ObjectMgr::Clear_All()
{
	for (size_t i = 0; i < static_cast<int>(OBJID::ID_END); ++i)
	{
		for (auto iter_begin = m_ObjectList[i].begin(); iter_begin != m_ObjectList[i].end(); ++iter_begin)
		{
			SAFE_DELETE_PTR(*iter_begin);
		}

		m_ObjectList[i].clear();
	}
}
