#pragma once
#include "ObjectMgr.h"
// Abstract Class
// 헤더파일 선언없이 전방선언만 한다.
class Scene abstract
{
public:
	Scene();
	virtual ~Scene();

public:
	virtual HRESULT InitializeScene() = 0;
	virtual void	UpdateScene(const float& fTimeDelta) = 0;
	virtual void	ReleaseScene() = 0;

protected:
	ObjectMgr* m_pObjectMgr;
};

