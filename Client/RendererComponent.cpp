#include "framework.h"
#include "RendererComponent.h"
#include "GameObject.h"

RendererComponent* RendererComponent::m_pInstance = nullptr;
RendererComponent::RendererComponent()
{
}

RendererComponent::~RendererComponent()
{
	ReleaseComponent();
}

HRESULT RendererComponent::InitializeComponent()
{
	return S_OK;
}

void RendererComponent::UpdateComponent(const float& fTimeDelta)
{
	//// 한번 렌더하고 다 지워주고
	//for (int i = 0; i < static_cast<int>(OBJLAYER::OBJ_END); ++i)
	//{
	//	this->RenderLayer(i);

	//	m_ObjectList[i].clear();
	//}
}

void RendererComponent::ReleaseComponent()
{
	ClearRenderLayer();
}

void RendererComponent::AddRenderLayer(const OBJLAYER& eLayer, GameObject* pObj)
{
	if (pObj == nullptr)
		return;

	m_ObjectList[static_cast<int>(eLayer)].push_back(pObj);
}

void RendererComponent::ClearRenderLayer()
{
	for (int i = 0; i < static_cast<int>(OBJLAYER::OBJ_END); ++i)
	{
		m_ObjectList[i].clear();
	}
}

void RendererComponent::RenderLayer(HDC hDC)
{
	for (int i = 0; i < static_cast<int>(OBJLAYER::OBJ_END); ++i)
	{
		for (auto& pObj : m_ObjectList[i])
			pObj->Render(hDC);

		m_ObjectList[i].clear();
	}
}

RendererComponent* RendererComponent::GetInstance()
{
	if (m_pInstance == nullptr)
	{
		m_pInstance = new RendererComponent();
	}

	return m_pInstance;
}

void RendererComponent::DestroyInstance()
{
	if (m_pInstance != nullptr)
	{
		SAFE_DELETE_PTR(m_pInstance);
	}
}
