#pragma once
#include "Component.h"

class TransformComponent final : public Component
{
private:
	explicit TransformComponent();
	explicit TransformComponent(const FPOINT& fInfo, const float& fVel, const float& fSize);
	virtual ~TransformComponent();

public:
	void	SetPosition(const FPOINT& tInfo);
	void	SetSize(const float& fSize);

	FPOINT	GetPosition() const;
	float	GetSize() const;

	void	MoveUp();
	void	MoveDown();
	void	MoveLeft();
	void	MoveRight();

public:
	virtual HRESULT	InitializeComponent() override;
	virtual void	UpdateComponent(const float& fTimeDelta) override;
	virtual void	ReleaseComponent() override;

public:
	static TransformComponent* CreateComponent(const FPOINT& fInfo, const float& fVel, const float& fSize);

private:
	FPOINT m_tInfo;
	float m_Size;
	float m_Velocity;
	float m_Acceleration;
	// float m_Angle
};