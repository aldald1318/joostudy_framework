#pragma once
#include "Scene.h"

//final 더이상 상속받을 애가 없다.
class SceneLogo final : public Scene
{
public:
	SceneLogo();
	virtual ~SceneLogo();

	// Scene을(를) 통해 상속됨
	virtual HRESULT InitializeScene() override;
	virtual void UpdateScene(const float& fTimeDelta) override;
	virtual void ReleaseScene() override;
};

