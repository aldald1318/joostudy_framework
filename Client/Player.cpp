#include "framework.h"
#include "Player.h"
#include "TransformComponent.h"
#include "RendererComponent.h"

Player::Player()
{
}

Player::~Player()
{
	this->Release();
}

HRESULT Player::Initialize()
{
	m_TransformComp = TransformComponent::CreateComponent(FPOINT(100.f, 100.f), 10.f, 10.f);

	GameObject::AddComponent(L"Transform_Player",m_TransformComp);
	//GameObject::AddComponent(L"Renderer_Player", RendererComponent::GetInstance());

	return S_OK;
}

int Player::Update(const float& DeltaTime)
{
	GameObject::UpdateComponents(DeltaTime);
	RendererComponent::GetInstance()->AddRenderLayer(RendererComponent::OBJLAYER::OBJ_PRIORITY, this);

	if (GetAsyncKeyState('W') & 0x8000) m_TransformComp->MoveUp();
	if (GetAsyncKeyState('A') & 0x8000) m_TransformComp->MoveLeft();
	if (GetAsyncKeyState('S') & 0x8000) m_TransformComp->MoveDown();
	if (GetAsyncKeyState('D') & 0x8000) m_TransformComp->MoveRight();

	return 0;
}

void Player::Render(HDC hDC)
{
	float size = m_TransformComp->GetSize();
	float fx = m_TransformComp->GetPosition().x;
	float fy = m_TransformComp->GetPosition().y;

	int left = static_cast<int>(fx - size);
	int top = static_cast<int>(fy - size);
	int right = static_cast<int>(fx + size);
	int bottom = static_cast<int>(fy + size);

	Rectangle(hDC, left, top, right, bottom);
}

void Player::Release()
{
	GameObject::ClearComponents();
}
