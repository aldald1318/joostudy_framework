#pragma once
#include "TemplateSingleton.h"

class GameObject;
class ObjectMgr final : public TemplateSingleton<ObjectMgr>
{
public:
	// Layer (enum class)묵시적 형변환을 해줌
	enum class OBJID { ID_PLAYER, ID_BULLET, ID_MONSTER, ID_END /*End -> Size*/ };

public:
	ObjectMgr();
	~ObjectMgr();

public:
	void Add_Object(OBJID eID, GameObject* pObj);
	void Update_ObjectMgr(const float& DeltaTime);
	void Clear_Filter(OBJID eID);
	void Clear_All();

public:
	// 중재자 패턴(인덱스로 관리) / 이터레이터 패턴
	STD list<GameObject*>	m_ObjectList[static_cast<int>(OBJID::ID_END)];

};

