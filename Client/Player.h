#pragma once
#include "GameObject.h"

class TransformComponent;
class Player final : public GameObject
{
public:
	explicit Player();
	virtual ~Player();

public:
	// GameObject을(를) 통해 상속됨
	virtual HRESULT	 Initialize() override;
	virtual int		 Update(const float& DeltaTime) override;
	virtual void	 Render(HDC hDC) override;

private:
	virtual void	 Release() override;

private:
	TransformComponent* m_TransformComp;
};

