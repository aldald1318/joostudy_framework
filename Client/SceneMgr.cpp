#include "framework.h"
#include "SceneMgr.h"
#include "Scene.h"

SceneMgr::SceneMgr() : m_pCurScene(nullptr)
{
}

SceneMgr::~SceneMgr()
{
	m_pCurScene->ReleaseScene();
	SAFE_DELETE_PTR(m_pCurScene);
}

HRESULT SceneMgr::SceneChange(Scene* pNewScene)
{
	// 인자로 들어온 pNewScene 객체를 현재 씬으로 할당합니다.
	if (m_pCurScene == nullptr)
		m_pCurScene = pNewScene;
	
	else 
	{
		SAFE_DELETE_PTR(m_pCurScene);
		m_pCurScene = pNewScene;
	}

	return S_OK;
}

void SceneMgr::UpdateScene(const float& fTimeDelta)
{
	if (m_pCurScene != nullptr)
		m_pCurScene->UpdateScene(fTimeDelta);
}

Scene* SceneMgr::GetCurScene() const
{
	return m_pCurScene;
}