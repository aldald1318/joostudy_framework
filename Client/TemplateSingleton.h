#pragma once
// Template Singleton

template <typename Type>
class TemplateSingleton
{
protected:
	// 持失切 社瑚切 private
	TemplateSingleton() {};
	virtual ~TemplateSingleton() {};

public:
	static Type* GetInstance()
	{
		if (m_pInstance == nullptr)
			m_pInstance = new Type;
		return m_pInstance;
	};

	static void DestroyInstance()
	{
		SAFE_DELETE_PTR(m_pInstance);
	};

private:
	static Type*		m_pInstance;
};

template <typename Type> Type* TemplateSingleton<Type>::m_pInstance = nullptr;