#pragma once

class Component;
class GameObject abstract
{
public:
	// exxplicit 찾아보기
	explicit GameObject();
	virtual ~GameObject();

public:
	virtual HRESULT Initialize() = 0;
	virtual int		Update(const float& DeltaTime) = 0;
	virtual void	Render(HDC hDC) = 0;

	void AddComponent(TCHAR* pKey, Component* pValue);
	void UpdateComponents(const float& fTimeDelta);
	void ClearComponents();

private:
	virtual void Release() = 0;

protected:
	// 우선 비워둠 (컴포넌트 컨테이너가 추가될 예정)
	std::unordered_map<TCHAR*, Component*> m_Components;
};

