﻿// Client.cpp : 애플리케이션에 대한 진입점을 정의합니다.
//

#include "framework.h"
#include "Client.h"
#include "MainFramework.h"

#define MAX_LOADSTRING 100

// 전역 변수:
HINSTANCE	hInst;                                // 현재 인스턴스입니다.
HWND		g_hWnd;
WCHAR		szTitle[MAX_LOADSTRING];                  // 제목 표시줄 텍스트입니다.
WCHAR		szWindowClass[MAX_LOADSTRING];            // 기본 창 클래스 이름입니다.

// 이 코드 모듈에 포함된 함수의 선언을 전달합니다:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    // TODO: 여기에 코드를 입력합니다.

    // 전역 문자열을 초기화합니다.
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDC_CLIENT, szWindowClass, MAX_LOADSTRING);
    MyRegisterClass(hInstance);

    // 애플리케이션 초기화를 수행합니다:
    if (!InitInstance (hInstance, nCmdShow))
    {
        return FALSE;
    }

    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_CLIENT));

    MSG msg;
	msg.message = WM_NULL;

	HDC hDC;
	hDC = GetDC(g_hWnd);

	MainFramework* pFramework = new MainFramework;
	pFramework->Initialize();	

	ULONGLONG ullOldTime = GetTickCount64();
	ULONGLONG ullOldTime2 = GetTickCount64();
	ULONGLONG ullFrame = 10; 
	/*자동형변환 항상 생각하기*/
	/*상수는 항상 변수화 해놓기*/

	int frameCnt = 0;

	while (msg.message != WM_QUIT)
	{
		// 기본 메시지 루프입니다:
		if (PeekMessage(&msg, nullptr, 0, 0, PM_REMOVE))
		{
			if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			// 1초에 약 60의 프레임으로 고정합니다.
			if (GetTickCount64() - ullOldTime >= ullFrame)
			{
				// 여기에서 프레임워크를 돌려줍니다.
				pFramework->Update_Framework(1.f);
				pFramework->Render_Framework(hDC);

				++frameCnt;

				ullOldTime = GetTickCount64();
			}

			if (GetTickCount64() - ullOldTime2 >= 1000)
			{
				TCHAR szFPS[12]{};
				wsprintf(szFPS, L"FPS: %d", frameCnt);
				SetWindowText(g_hWnd, szFPS);

				frameCnt = 0;

				ullOldTime2 = GetTickCount64();
			}
		}
	}

	SAFE_DELETE_PTR(pFramework);

    return (int) msg.wParam;
}

//
//  함수: MyRegisterClass()
//
//  용도: 창 클래스를 등록합니다.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

	/* 게임이름 게임아이콘 바꾸면 플러스점수 */
    wcex.style          = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc    = WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_CLIENT));
    wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground  = (HBRUSH)(COLOR_WINDOW+1);
    wcex.lpszMenuName   = nullptr;
    wcex.lpszClassName  = szWindowClass;
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

    return RegisterClassExW(&wcex);
}

//
//   함수: InitInstance(HINSTANCE, int)
//
//   용도: 인스턴스 핸들을 저장하고 주 창을 만듭니다.
//
//   주석:
//
//        이 함수를 통해 인스턴스 핸들을 전역 변수에 저장하고
//        주 프로그램 창을 만든 다음 표시합니다.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   hInst = hInstance; // 인스턴스 핸들을 전역 변수에 저장합니다.

   RECT rc = {0,0,WINDOW_SIZE_X, WINDOW_SIZE_Y};
   DWORD dwStyle = WS_OVERLAPPEDWINDOW;

   // 메뉴창을 제외한 그리는 화면의 크기를 정해진 크기로 맞춰준다.
   AdjustWindowRect(&rc, dwStyle, FALSE);

   g_hWnd = CreateWindowW(szWindowClass, szTitle, dwStyle,
      0, 0, // 창의 생성위치 (LT 기준)
	  (rc.right-rc.left), (rc.bottom - rc.top), // 창의 크기
	  nullptr, nullptr, hInstance, nullptr);

   if (!g_hWnd)
   {
      return FALSE;
   }

   ShowWindow(g_hWnd, nCmdShow);
   UpdateWindow(g_hWnd);

   return TRUE;
}

//
//  함수: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  용도: 주 창의 메시지를 처리합니다.
//
//  WM_DESTROY  - 종료 메시지를 게시하고 반환합니다.
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch (message)
    {
	case WM_KEYDOWN:
		if(wParam == VK_ESCAPE)
			PostQuitMessage(0);
		break;
    case WM_DESTROY:
        PostQuitMessage(0);
        break;
    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
    return 0;
}