#pragma once

class SceneMgr;
class RendererComponent;
class MainFramework
{
public:
	MainFramework();
	virtual ~MainFramework();

public:
	HRESULT Initialize();
	void Update_Framework(const float& fTimeDelta);
	void Render_Framework(HDC hDC);

private:
	void	Release();

private:
	SceneMgr* m_pSceneMgr;
	RendererComponent* m_pRendererComp;
};

