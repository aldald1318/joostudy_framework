#pragma once

#define WINDOW_SIZE_X 800
#define WINDOW_SIZE_Y 600

#define SAFE_DELETE_PTR(ptr)	\
{								\
	if (ptr != nullptr)			\
	{							\
		delete ptr;				\
		ptr = nullptr;			\
	}							\
}								

typedef struct tag_point
{
	float x;
	float y;

	tag_point() : x(0.f), y(0.f) {};
	tag_point(const float& _x, const float& _y) : x(_x), y(_y) {};
	~tag_point() = default;
} FPOINT;