#include "framework.h"
#include "MainFramework.h"

#include "SceneMgr.h"
#include "SceneLogo.h"

#include "RendererComponent.h"

MainFramework::MainFramework() : m_pSceneMgr(nullptr), m_pRendererComp(nullptr)
{
}

MainFramework::~MainFramework()
{
	Release();
}

HRESULT MainFramework::Initialize()
{
	// 최초의 SceneMgr의 Instance주소를 저장해놓는다.
	m_pSceneMgr = SceneMgr::GetInstance();

	if (m_pSceneMgr == nullptr)
		return E_FAIL;

	Scene* pLogoScene = new SceneLogo;
	pLogoScene->InitializeScene();

	m_pSceneMgr->SceneChange(pLogoScene);

	// 렌더러 생성
	m_pRendererComp = RendererComponent::GetInstance();

	return S_OK;
}

void MainFramework::Update_Framework(const float& fTimeDelta)
{
	if (m_pSceneMgr != nullptr)
		m_pSceneMgr->UpdateScene(fTimeDelta);
}

void MainFramework::Render_Framework(HDC hDC)
{
	Rectangle(hDC, 0, 0, WINDOW_SIZE_X, WINDOW_SIZE_Y); // 잔상제거용
	if (m_pRendererComp != nullptr)
		m_pRendererComp->RenderLayer(hDC);
}

void MainFramework::Release()
{
	m_pSceneMgr->DestroyInstance();
	m_pRendererComp->DestroyInstance();
}
